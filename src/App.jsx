import { useState } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import Layout from "./layout/Layout";
import About from "./pages/About/About";
import HomePage from "./pages/HomePage/HomePage";
import "animate.css";
import Contact from "./pages/Contact/Contact";

function App() {
  return (
    <div
      className="App relative bg-cover bg-fixed bg-center h-screen"
      style={{ backgroundImage: "url('./assets/finland.jpg')" }}
    >
      <BrowserRouter>
        <Routes>
          <Route
            path="/"
            element={
              <Layout>
                <HomePage />
              </Layout>
            }
          />
          <Route
            path="/About"
            element={
              <Layout>
                <About />
              </Layout>
            }
          />
          <Route
            path="/Contact"
            element={
              <Layout>
                <Contact />
              </Layout>
            }
          />
        </Routes>
      </BrowserRouter>{" "}
      <div class="absolute inset-0 bg-black opacity-25 z-[-1]"></div>
    </div>
  );
}

export default App;
