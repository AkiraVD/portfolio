import React, { useState } from "react";
import { message } from "antd";
import { services } from "../../utils/api";

export default function Contact() {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [messager, setMessenger] = useState("");
  const [sending, setsending] = useState(false);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setsending(true);
    await services
      .getMsg({ name, email, message: messager })
      .then((res) => {
        message.success(
          "Thank you for your message! I appreciate it and will get back to you as soon as possible"
        );
        setsending(false);
        services.sendReply({ name, email });
      })
      .catch((err) => {
        message.error(err.response.data);
        setsending(false);
      });
  };
  return (
    <div className="background-img contact pt-[100px] min-h-[calc(100vh-72px)] p-5">
      <div className="container mx-auto bg-[rgba(0,0,0,0.5)] p-8 rounded-xl">
        <h1 className="text-2xl font-bold mb-4 text-yellow-600">Contact Me</h1>
        <form onSubmit={handleSubmit}>
          <div className="mb-4">
            <label htmlFor="name" className="block font-medium mb-2 text-white">
              Name
            </label>
            <input
              type="text"
              id="name"
              className="block w-full p-2 border rounded-md"
              required
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
          </div>
          <div className="mb-4">
            <label
              htmlFor="email"
              className="block font-medium mb-2 text-white"
            >
              Email
            </label>
            <input
              type="email"
              id="email"
              className="block w-full p-2 border rounded-md"
              required
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div className="mb-4">
            <label
              htmlFor="message"
              className="block font-medium mb-2 text-white"
            >
              Message
            </label>
            <textarea
              id="message"
              className="block w-full p-2 border rounded-md"
              rows="5"
              required
              value={messager}
              onChange={(e) => setMessenger(e.target.value)}
            ></textarea>
          </div>
          <div className="text-center">
            <button
              type="submit"
              className={`text-white py-2 px-4 rounded-md duration-300 ${
                sending
                  ? "bg-gray-500 hover:bg-gray-600"
                  : "bg-yellow-500 hover:bg-yellow-600"
              }`}
              disabled={sending}
            >
              {sending ? "Submiting..." : "Submit"}
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}
