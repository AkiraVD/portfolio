import React from "react";

export default function About() {
  return (
    <div className="background-img about pt-[100px] min-h-[calc(100vh-72px)] flex flex-col gap-4 p-5">
      <div className="container mx-auto bg-[rgba(0,0,0,0.5)] p-5 rounded-xl  animate__animated animate__fadeInRight hover:drop-shadow-lg hover:shadow-lg duration-300">
        <h2 className="font-bold text-green-500 text-4xl">Who am I</h2>
        <p className="text-white ml-10 my-5 text-left">
          A small developer from Viet Nam who may have come to coding late, but
          I has a strong passion for it.
          <br />
          My personality is that of an introverted and minimalist individual,
          but I'm able to socialize effectively with colleagues."
          <br />
        </p>
      </div>
      <div className="container mx-auto bg-[rgba(0,0,0,0.5)] p-5 rounded-xl  animate__animated animate__fadeInLeft animate__delay-1s hover:drop-shadow-lg hover:shadow-lg duration-300">
        <h2 className="font-bold text-green-500 text-4xl text-right">
          Why did I choose coding
        </h2>
        <p className="text-white mr-10 my-5 text-right">
          Ever since I was a little kid, I've had a strong interest in computers
          in general.
          <br /> However, after graduating from high school, I was a bit lost
          and didn't know what to do, so I ended up learning about various
          things that weren't my passion. <br /> Then, the pandemic happened and
          it gave me a chance to turn my life around. So, I finally decided to
          follow my dream of working in the technology industry.
        </p>
      </div>
      <div className="container mx-auto bg-[rgba(0,0,0,0.5)] p-5 rounded-xl  animate__animated animate__fadeInRight animate__delay-2s hover:drop-shadow-lg hover:shadow-lg duration-300">
        <h2 className="font-bold text-green-500 text-4xl">My hobby</h2>
        <p className="text-white ml-10 my-5 text-left">
          In my free time, I enjoy playing games, following the latest news and
          updates in the field of technology, and reading manga. <br /> I have a
          special interest in Japanese culture, having lived in Osaka for two
          years. During that time, I gained a deeper understanding and
          appreciation of their traditions and way of life. <br /> Overall,
          these hobbies and interests reflect my love for both entertainment and
          learning.
        </p>
      </div>
    </div>
  );
}
