import React, { useState } from "react";

export default function Banner() {
  return (
    <div className="background-img home bg-cover bg-fixed bg-center">
      <div className="container mx-auto h-[800px] p-10 flex flex-col justify-center items-center">
        <div className="relative h-[200px] w-[200px] flex rounded-full overflow-hidden animate__animated animate__fadeIn animate__delay-1s">
          <img
            src="https://file.akiravd.online/images/assets/megane-shiba.jpg"
            alt="profile-pic"
            className="object-cover duration-300"
          />
          <div className="absolute inset-0 opacity-0 hover:opacity-100 duration-500">
            <img
              src="https://file.akiravd.online/images/assets/profile-pic.jpg"
              alt="profile-pic"
              className="object-cover duration-300"
            />
          </div>
        </div>
        <div className="flex flex-col justify-center items-center gap-3 mt-4">
          <h1 className="text-4xl text-white font-semibold animate__animated animate__fadeInRight animate__delay-1s">
            LÊ DANH PHƯƠNG
          </h1>
          <h2 className="text-2xl text-gray-300 animate__animated animate__fadeInLeft animate__delay-1s">
            HI, I'M A <span className="text-green-500">FULL-STACK</span>{" "}
            DEVELOPER
          </h2>
          <div className="text-white text-2xl flex justify-center items-center gap-4 animate__animated animate__fadeInUp animate__delay-2s">
            <a
              href="https://gitlab.com/AkiraVD"
              target="_blank"
              className="hover:text-green-500 duration-300"
            >
              <i class="fab fa-gitlab"></i>
            </a>
            <a
              href="https://www.linkedin.com/in/le-danh-phuong-077826247/"
              className="hover:text-green-500 duration-300"
              target="_blank"
            >
              <i class="fab fa-linkedin-in"></i>
            </a>
            <a
              href="mailto:ledanhphuong@gmail.com"
              className="hover:text-green-500 duration-300"
            >
              <i class="fa fa-envelope"></i>
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}
