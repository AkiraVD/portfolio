import React from "react";
import Banner from "./Banner/Banner";
import Project from "./Project/Project";
import Specilities from "./Specilities/Specilities";

export default function HomePage() {
  return (
    <div className="">
      <Banner />
      <Specilities />
      <Project />
    </div>
  );
}
