import React from "react";
import { Tabs } from "antd";
import { skillFE, skillBE, skillOther } from "../../../utils/skills";

export default function Specilities() {
  const handleRenderSkills = (skills) => {
    return (
      <div className="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-5 xl:grid-cols-6 gap-5 lg:min-h-[400px] md:min-h-[610px]">
        {skills.map((skill) => {
          return (
            <div className="flex flex-col justify-center items-center gap-5 p-5 rounded-lg glassmorphism text-white hover:bg-[rgba(0,0,0,.7)] hover:shadow-xl hover:translate-y-[-5px] duration-500">
              <div className="w-[100px] aspect-square relative overflow-hidden">
                <img
                  src={skill.icon}
                  alt={skill.name}
                  className="object-contain absolute inset-0 w-full h-full"
                />
              </div>
              <p className="text-xl font-bold">{skill.name}</p>
            </div>
          );
        })}
      </div>
    );
  };

  const items = [
    {
      key: "1",
      label: (
        <h4
          className="text-xl font-semibold text-green-500"
          style={{ textShadow: "5px 5px 5px #000" }}
        >
          Front-end
        </h4>
      ),
      children: <>{handleRenderSkills(skillFE)}</>,
    },
    {
      key: "2",
      label: (
        <h4
          className="text-xl font-semibold text-green-500"
          style={{ textShadow: "5px 5px 5px #000" }}
        >
          Back-end
        </h4>
      ),
      children: <>{handleRenderSkills(skillBE)}</>,
    },
    {
      key: "3",
      label: (
        <h4
          className="text-xl font-semibold text-green-500"
          style={{ textShadow: "5px 5px 5px #000" }}
        >
          Others
        </h4>
      ),
      children: <>{handleRenderSkills(skillOther)}</>,
    },
  ];
  return (
    <div
      className="border-t-4 border-b-4 w-full py-5 bg-cover bg-fixed"
      style={{
        backgroundImage: 'url("https://file.akiravd.online/images/skill.jpg")',
      }}
    >
      <div className="container mx-auto p-3 animate__animated  animate__fadeInUp animate__delay-2s">
        <h2
          className="text-xl font-bold text-center text-green-500"
          style={{ textShadow: "0px 0px 10px #555" }}
        >
          Specilities
        </h2>
        <h3
          className="text-3xl font-bold text-center text-white my-3"
          style={{ textShadow: "0px 0px 10px #000" }}
        >
          What skills do I have
        </h3>
        <Tabs defaultActiveKey="1" items={items} centered />
      </div>
    </div>
  );
}
