import React from "react";
import { Link } from "react-router-dom";
import { projects } from "../../../utils/projects";

export default function Project() {
  const handleRenderProject = (arr) => {
    return arr.map((item, index) => {
      return (
        <div
          className={`relative glassmorphism rounded overflow-hidden w-full aspect-video animate__animated animate__delay-3s ${
            index % 2 == 0 ? "animate__fadeInLeft" : "animate__fadeInRight"
          }`}
        >
          <div className="absolute inset-0 p-8 flex flex-col backdrop-blur-sm bg-black bg-opacity-50 opacity-0 hover:opacity-100 duration-500">
            <h4 className="text-green-500 text-xl md:text-2xl 2xl:text-3xl font-bold">
              {item.title}
            </h4>
            <p className="text-white text-sm md:text-xl my-3">
              {item.description}
            </p>
            <Link to={item.link} target="_blank">
              {item.live ? (
                <button className="bg-yellow-500 py-2 px-4 rounded border-2 border-yellow-500 hover:bg-[rgba(0,0,0,0.5)] hover:text-yellow-500 duration-500 font-semibold">
                  Live Demo
                </button>
              ) : (
                <button className="bg-red-500 py-2 px-4 rounded border-2 border-red-500 hover:bg-[rgba(0,0,0,0.5)] hover:text-yellow-500 duration-500 font-semibold">
                  Demo Video
                </button>
              )}
            </Link>
            <div
              className={
                `absolute  origin-right -rotate-45 top-[20px] -right-[100px] w-full text-center ` +
                item.color
              }
            >
              <p className="text-xl font-semibold text-white">{item.core}</p>
            </div>
          </div>
          <img
            src={item.img}
            alt={item.title}
            className="object-contain w-full h-full duration-300"
          />
        </div>
      );
    });
  };
  return (
    <div
      className="relative bg-cover bg-fixed bg-center"
      style={{ backgroundImage: 'url("/project.png")' }}
    >
      <div className="container mx-auto py-5">
        <h2 className="text-xl font-bold text-center text-white animate__animated animate__fadeInDown animate__delay-3s">
          Portfolio
        </h2>
        <h3 className="text-3xl font-bold text-center text-green-600 my-3 animate__animated animate__fadeInDown animate__delay-3s">
          My latest projects
        </h3>
        <div className="grid grid-cols-1 lg:grid-cols-2 2xl:grid-cols-3 gap-5 mt-10 px-5">
          {handleRenderProject(projects.slice(0, 6))}
        </div>
      </div>
    </div>
  );
}
