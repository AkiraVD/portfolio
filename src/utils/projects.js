export const projects = [
  {
    title: "Fiverr API",
    description: "Backend server for Fiver App written in NestJS",
    img: "https://file.akiravd.online/images/project/fiverr.png",
    link: "https://api-fiverr.onrender.com/api-docs",
    core: "NestJS",
    color: "bg-red-500",
    live: false,
  },
  {
    title: "Picpost",
    description:
      "A pictures sharing website where you can post pictures, like and comments",
    img: "https://file.akiravd.online/images/project/picpost.png",
    link: "https://picpost.vercel.app/",
    core: "Full-stack",
    color: "bg-yellow-500",
    live: true,
  },
  {
    title: "E-learning",
    description:
      "An online learning website especially for coding, with various features like registering users, creating new courses, etc.",
    img: "https://file.akiravd.online/images/project/elearning.png",
    link: "https://final-project-elearning.vercel.app",
    core: "ReactJS",
    color: "bg-blue-500",
    live: true,
  },
  {
    title: "Myflix",
    description:
      "A cinema website where you can find more informations about the newest movies and book tickets",
    img: "https://file.akiravd.online/images/project/cinema.png",
    link: "https://akiravd-myflix.netlify.app",
    core: "ReactJS",
    color: "bg-blue-500",
    live: true,
  },
  {
    title: "Simple Phone Store",
    description:
      "A phone store which you can check the specs and add the phone to card , also with management site where you can add more phone on to the shop",
    img: "https://file.akiravd.online/images/project/phone-store.png",
    link: "https://akiravd-phone-store.netlify.app",
    core: "Bootstrap",
    color: "bg-purple-500",
    live: true,
  },
  {
    title: "Mastery (Layout)",
    description:
      "Layout for Masterwork website, with various effect and change theme button",
    img: "https://file.akiravd.online/images/project/mastery.png",
    link: "https://akiravd-mastery.netlify.app",
    core: "Bootstrap",
    color: "bg-purple-500",
    live: true,
  },
  {
    title: "Music Player",
    description: "Simple music player written in javascript",
    img: "https://file.akiravd.online/images/project/music-player.png",
    link: "https://akirify.netlify.app/",
    core: "Javascript",
    color: "bg-yellow-500",
    live: true,
  },
];
