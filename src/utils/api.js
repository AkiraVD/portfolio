import axios from "axios";

const BASE_URL = "https://message-vjpy.onrender.com/";

const http = axios.create({
  baseURL: BASE_URL,
});

export const services = {
  getMsg: (data) => {
    return http.post("getMsg", data);
  },
  sendReply: (data) => {
    return http.post("sendReply", data);
  },
};
