export const skillFE = [
  {
    name: "HTML",
    icon: "https://file.akiravd.online/images/skills/html-5.png",
  },
  { name: "CSS", icon: "https://file.akiravd.online/images/skills/css-3.png" },
  { name: "Sass", icon: "https://file.akiravd.online/images/skills/sass.png" },
  {
    name: "Bootstrap",
    icon: "https://file.akiravd.online/images/skills/bootstrap.png",
  },
  {
    name: "TailwindCSS",
    icon: "https://file.akiravd.online/images/skills/tailwindcss.png",
  },
  {
    name: "Ant Design",
    icon: "https://file.akiravd.online/images/skills/ant-design.png",
  },
  {
    name: "Axios",
    icon: "https://file.akiravd.online/images/skills/axios.png",
  },
  {
    name: "ReactJS",
    icon: "https://file.akiravd.online/images/skills/react.png",
  },
  {
    name: "Redux",
    icon: "https://file.akiravd.online/images/skills/redux.png",
  },
];
export const skillBE = [
  {
    name: "NodeJS",
    icon: "https://file.akiravd.online/images/skills/node-js.png",
  },
  {
    name: "Express JS",
    icon: "https://file.akiravd.online/images/skills/js.png",
  },
  {
    name: "SQL",
    icon: "https://file.akiravd.online/images/skills/sql-server.png",
  },
  {
    name: "Sequelize",
    icon: "https://file.akiravd.online/images/skills/sequelize.png",
  },
  {
    name: "Prisma",
    icon: "https://file.akiravd.online/images/skills/prisma.png",
  },
  {
    name: "NestJS",
    icon: "https://file.akiravd.online/images/skills/nestjs.png",
  },
];
export const skillOther = [
  {
    name: "JavaScript",
    icon: "https://file.akiravd.online/images/skills/js.png",
  },
  {
    name: "TypeScript",
    icon: "https://file.akiravd.online/images/skills/typescript.png",
  },
  { name: "GIT", icon: "https://file.akiravd.online/images/skills/git.png" },
  {
    name: "Docker",
    icon: "https://file.akiravd.online/images/skills/docker.png",
  },
  {
    name: "Postman",
    icon: "https://file.akiravd.online/images/skills/postman.png",
  },
  {
    name: "Japanese",
    icon: "https://file.akiravd.online/images/skills/japanese.svg",
  },
];
