import React from "react";

export default function Footer() {
  return (
    <div className="bg-black">
      <div className="container mx-auto p-5 flex justify-between items-center">
        <p className="text-white text-center">
          Design by <span className="text-red-500">myself</span>{" "}
          <span className="text-black hover:text-gray-500 duration-300">
            with various references
          </span>
        </p>
        <div className="text-white text-2xl flex justify-center items-center gap-4">
          <a href="https://gitlab.com/AkiraVD" target="_blank">
            <i class="fab fa-gitlab"></i>
          </a>
          <a
            href="https://www.linkedin.com/in/le-danh-phuong-077826247/"
            target="_blank"
          >
            <i class="fab fa-linkedin-in"></i>
          </a>
          <a href="mailto:ledanhphuong@gmail.com">
            <i class="fa fa-envelope"></i>
          </a>
        </div>
      </div>
    </div>
  );
}
