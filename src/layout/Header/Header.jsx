import React from "react";
import { Link, NavLink } from "react-router-dom";
export default function Header() {
  const activeClassName = "text-green-500 before:w-full";
  const inactiveClassName = "before:w-0";
  const baseClassName =
    "relative before:block before:absolute before:bottom-[-5px] before:left-0 before:w-0 before:h-[2px] before:bg-green-500 before:duration-300 hover:before:w-full hover:duration-300 ";
  return (
    <div className="container mx-auto ">
      <div className="flex justify-between items-center ">
        <Link to="/">
          <div className="h-[80px] w-[200px] bg-transparent hidden sm:flex justify-center items-center p-3">
            <img
              src="https://file.akiravd.online/images/assets/logo.png"
              alt="logo"
              className="object-contain"
            />
          </div>
        </Link>
        <nav className="mr-10 text-white text-xl w-full sm:w-auto p-5 sm:p-0">
          <ul className="flex gap-10">
            <li>
              <NavLink
                exact="true"
                to="/"
                className={({ isActive }) =>
                  isActive
                    ? baseClassName + activeClassName
                    : baseClassName + inactiveClassName
                }
              >
                Home
              </NavLink>
            </li>
            <li>
              <NavLink
                to="/About"
                className={({ isActive }) =>
                  isActive
                    ? baseClassName + activeClassName
                    : baseClassName + inactiveClassName
                }
              >
                About me
              </NavLink>
            </li>
            <li>
              <NavLink
                to="/Contact"
                href="#footer"
                className={({ isActive }) =>
                  isActive
                    ? baseClassName + activeClassName
                    : baseClassName + inactiveClassName
                }
              >
                Contact
              </NavLink>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  );
}
