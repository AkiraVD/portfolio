import React, { useRef } from "react";
import Footer from "./Footer/Footer";
import Header from "./Header/Header";

export default function Layout({ children }) {
  const scrollRef = useRef(null);
  const scrollToBottom = () => {
    scrollRef.current.scrollTo({
      top: scrollRef.current.scrollHeight,
      behavior: "smooth",
    });
  };
  return (
    <div className="flex flex-col min-h-screen relative">
      <div className="absolute w-full z-[100]">
        <Header scrollTo={scrollToBottom} />
      </div>
      <div ref={scrollRef} className="grow">
        {children}
      </div>
      <Footer />
    </div>
  );
}
