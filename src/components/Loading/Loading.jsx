import React from "react";
import "./Loading.css";
export default function Loading() {
  return (
    <div className="w-full flex justify-center items-center bg-gray-300">
      <div className="lds-ring">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
    </div>
  );
}
